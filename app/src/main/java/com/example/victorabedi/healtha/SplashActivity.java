package com.example.victorabedi.healtha;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {
    ImageView main;
    Animation fabopen,fabclose,fabrotate,fabantirotate;
    boolean isOpen=false;
private static int SLASPH_TIME_OUT=4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        main=(ImageView)findViewById(R.id.main);
        fabopen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fabclose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        fabrotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        fabantirotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {

                    main.startAnimation(fabantirotate);

                    isOpen = false;
                } else {

                    main.startAnimation(fabrotate);

                    isOpen = true;


                }
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent next =new Intent(getApplicationContext(),MainActivity.class);
                startActivity(next);
                finish();
            }
        },SLASPH_TIME_OUT);
    }
}
