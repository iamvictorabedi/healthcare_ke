package com.example.victorabedi.healtha;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import static android.R.attr.id;

public class MainActivity3 extends AppCompatActivity   {
    FloatingActionButton fabplus,fabcare,fabdoc,fabinsurance;
    Animation fabopen,fabclose,fabrotate,fabantirotate;
    boolean isOpen=false;


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        fabplus = (FloatingActionButton) findViewById(R.id.fab);
        fabcare = (FloatingActionButton) findViewById(R.id.fab_emergency);
        fabdoc = (FloatingActionButton) findViewById(R.id.fab_getdoc);
        fabinsurance = (FloatingActionButton) findViewById(R.id.fab_Insurance);

        fabopen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fabclose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        fabrotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        fabantirotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);
        fabplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {
                    fabinsurance.startAnimation(fabclose);
                    fabcare.startAnimation(fabclose);
                    fabdoc.startAnimation(fabclose);
                    fabplus.startAnimation(fabantirotate);
                    fabinsurance.setClickable(false);
                    fabcare.setClickable(false);
                    fabdoc.setClickable(false);
                    isOpen = false;
                } else {
                    fabinsurance.startAnimation(fabopen);
                    fabcare.startAnimation(fabopen);
                    fabdoc.startAnimation(fabopen);
                    fabplus.startAnimation(fabrotate);
                    fabinsurance.setClickable(true);
                    fabcare.setClickable(true);
                    fabdoc.setClickable(true);
                    isOpen = true;


                }
                fabdoc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent scan = new Intent(getApplicationContext(), NotificationActivity.class);
                        startActivity(scan);
                    }
                });
                fabcare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent scan0 = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(scan0);
                    }
                });

            }
        });

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:

                    Tab1AlimentStatusooo tab1 = new Tab1AlimentStatusooo();

                    return tab1;
                case 1:
                    Tab2AlimentsFormoooo tab2 = new Tab2AlimentsFormoooo();

                    return tab2;
                case 2:
                    Tab3Favoritesoooo tab3 = new Tab3Favoritesoooo();
                    return tab3;


            }
            return null;
        }
        @Override
        public int getCount(){

            return 3;
        }
        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position) {
                case 0:
                    return "HealthCare Pro";
                case 1:
                    return "HealthCare Plus";
                case 2:
                    return "Kidogo Plus";
            }
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Offsite) {

            Intent x = new Intent(getApplicationContext(), MainActivity3.class);
            startActivity(x);
            return true;
        } else if (id == R.id.action_Share) {
            Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+254723795034"));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return true;
            }
            startActivity(call);
            return true;
        }
        else if (id == R.id.action_DevsSite) {
            Intent site = new Intent(Intent.ACTION_VIEW,Uri.parse("http://apphive.co.ke/medicalcare"));
            startActivity(site);
            return true;
        }
        else if (id == R.id.action_Review) {
            Intent share= new Intent(Intent.ACTION_SEND);
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT,"Get Convinient and Affordable Treatement at Your Place of Conformt without Visting A hospital. Download The HealthCare App on Google Play Store or use Link : www.healthcare.co.ke/app");
            startActivity(share);
            return true;
        }
        return true;
    }
private DrawerLayout drawer;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {


            case R.id.action_Offsite:
                Intent x = new Intent(getApplicationContext(), MainActivity3.class);
                startActivity(x);
                return true;
            case R.id.action_Share:
                Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+254723795034"));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return true;
                }
                startActivity(call);
                return true;
            case R.id.action_DevsSite:
                Intent site = new Intent(Intent.ACTION_VIEW,Uri.parse("http://apphive.co.ke/healthcareke"));
                startActivity(site);
                return true;
            case R.id.action_Review:
                Intent share= new Intent(Intent.ACTION_SEND);
                share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT,"Get Convinient and Affordable Treatement at Your Place of Conformt without Visting A hospital. Download The HealthCare App on Google Play Store or use Link : www.healthcare.co.ke/app");
                startActivity(share);
                return true;
            case R.id.action_notification:
                Intent not = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(not);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
