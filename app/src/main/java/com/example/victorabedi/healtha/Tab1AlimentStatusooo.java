package com.example.victorabedi.healtha;

/**
 * Created by VictorAbedi on 12/17/2016.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class Tab1AlimentStatusooo extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab1alimentstatusoooo, container, false);
        final ImageView me, us, them, we, they;

        me = (ImageView) rootView.findViewById(R.id.overflow);
        me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(me);
            }

        });
        us = (ImageView) rootView.findViewById(R.id.overflow);
        us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(us);
            }

        });
        them = (ImageView) rootView.findViewById(R.id.overflow);
        them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(them);
            }

        });
        we = (ImageView) rootView.findViewById(R.id.overflow);
        we.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(we);
            }

        });
        they = (ImageView) rootView.findViewById(R.id.overflow);
        they.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(they);
            }

        });

        return rootView;}

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(getActivity(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_doctor, popup.getMenu());
        popup.setOnMenuItemClickListener(new Tab1AlimentStatusooo.MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(getActivity(), "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_consult:
                    Toast.makeText(getActivity(), "Consult Doctor", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_appoint:
                    Toast.makeText(getActivity(), "Book Appointment", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

}