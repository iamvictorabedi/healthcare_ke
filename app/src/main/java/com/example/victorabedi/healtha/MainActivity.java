package com.example.victorabedi.healtha;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import static android.R.attr.id;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {



    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:

                    Tab1AlimentStatus tab1 = new Tab1AlimentStatus();

                    return tab1;
                case 1:
                    Tab2AlimentsForm tab2 = new Tab2AlimentsForm();

                    return tab2;
                case 2:
                    Tab3Favorites tab3 = new Tab3Favorites();
                    return tab3;


            }
            return null;
        }
        @Override
        public int getCount(){

            return 3;
        }
        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position) {
                case 0:
                    return "GET DOCTOR";
                case 1:
                    return "HEALTH TIPS";
                case 2:
                    return "eCare";
            }
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Offsite) {

            Intent x = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(x);
            return true;
        } else if (id == R.id.action_Share) {
            Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+254723795034"));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return true;
            }
            startActivity(call);
            return true;
        }
        else if (id == R.id.action_DevsSite) {
            Intent site = new Intent(Intent.ACTION_VIEW,Uri.parse("http://apphive.co.ke/medicalcare"));
            startActivity(site);
            return true;
        }
        else if (id == R.id.action_Review) {
            Intent share= new Intent(Intent.ACTION_SEND);
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT,"Get Convinient and Affordable Treatement at Your Place of Conformt without Visting A hospital. Download The HealthCare App on Google Play Store or use Link : www.healthcare.co.ke/app");
            startActivity(share);
            return true;
        }
        return true;
    }
private DrawerLayout drawer;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {


            case R.id.action_Offsite:
                Intent x = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(x);
                return true;
            case R.id.action_Share:
                Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+254723795034"));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return true;
                }
                startActivity(call);
                return true;
            case R.id.action_DevsSite:
                Intent site = new Intent(Intent.ACTION_VIEW,Uri.parse("http://apphive.co.ke/healthcareke"));
                startActivity(site);
                return true;
            case R.id.action_Review:
                Intent share= new Intent(Intent.ACTION_SEND);
                share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT,"Get Convinient and Affordable Treatement at Your Place of Conformt without Visting A hospital. Download The HealthCare App on Google Play Store or use Link : www.healthcare.co.ke/app");
                startActivity(share);
                return true;
            case R.id.action_notification:
                Intent not = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(not);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // Handle navigation view item clicks here.
        int id = menuItem.getItemId();
        switch (id) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_settings:
                return true;
        }
        android.app.FragmentManager fragmentManager= getFragmentManager();
        if (id == R.id.nav_profile) {
            // Handle the camera action

            Intent x = new Intent(getApplicationContext(),PostActivity.class);
            startActivity(x);
        } else if (id == R.id.nav_Insurance) {

            Intent x = new Intent(getApplicationContext(),MainActivity3.class);
            startActivity(x);
        } else if (id == R.id.nav_code) {

            Intent x = new Intent(getApplicationContext(),PromoCode.class);
            startActivity(x);
        } else if (id == R.id.nav_payment) {

            Intent x = new Intent(getApplicationContext(),PaymentTrends.class);
            startActivity(x);
        } else if (id == R.id.nav_doc) {

            Intent x = new Intent(getApplicationContext(),GetDoctorActivity.class);
            startActivity(x);
        } else if (id == R.id.nav_emergency) {

            Intent x = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(x);
        } else if (id == R.id.nav_healthtip) {
            Intent x = new Intent(getApplicationContext(),HealthTips.class);
            startActivity(x);
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_logout) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Are you Sure You Want To Logout??");
            alertDialogBuilder.setPositiveButton("No , Thanks",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Toast.makeText(MainActivity.this,"No Thanks",Toast.LENGTH_LONG).show();
                        }
                    });

            alertDialogBuilder.setNegativeButton("Yes, Logout",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
