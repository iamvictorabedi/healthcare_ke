package com.example.victorabedi.healtha;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {
    MaterialSearchView searchView;
    ListView lstview;
    String[] lstSource ={
        "User Profile","HealthCare Insurance","Promo Code","Payment Trend","Get A Doctor","Emergency Care Unit","HealthTips","Settings","Logout User"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Notification");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lstview=(ListView)findViewById(R.id.lstview);
        ArrayAdapter adapter= new ArrayAdapter(this,android.R.layout.simple_list_item_1,lstSource);
        lstview.setAdapter(adapter);
        searchView = (MaterialSearchView)findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText !=null && !newText.isEmpty()){


                    List<String> lstFound= new ArrayList<String>();
                    for(String item:lstSource){

                       if (item.contains(newText))
                           lstFound.add(item);

                    }
                    ArrayAdapter adapter= new ArrayAdapter(NotificationActivity.this,android.R.layout.simple_list_item_1,lstFound);
                    lstview.setAdapter(adapter);

                }
                else {

                    ArrayAdapter adapter= new ArrayAdapter(NotificationActivity.this,android.R.layout.simple_list_item_1,lstSource);
                    lstview.setAdapter(adapter);
                }
                return true;
            }

        });
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
//if closed listview will return default
                lstview=(ListView)findViewById(R.id.lstview);
                ArrayAdapter adapter= new ArrayAdapter(NotificationActivity.this,android.R.layout.simple_list_item_1,lstSource);
                lstview.setAdapter(adapter);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back,menu);
        MenuItem  item= menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;


    }
}

