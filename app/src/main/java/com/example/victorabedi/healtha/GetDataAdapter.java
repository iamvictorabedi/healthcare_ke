package com.example.victorabedi.healtha;

/**
 * Created by VictorAbedi on 1/7/2017.
 */


public class GetDataAdapter {


    public String DoctorName;
    public String Doctorprofile;

    public String ImageServerUrl;

    public String getImageServerUrl() {
        return ImageServerUrl;
    }

    public void setImageServerUrl(String imageServerUrl) {
        this.ImageServerUrl = imageServerUrl;
    }
    public String getDoctorName() {

        return DoctorName;
    }

    public void setDoctorName(String DoctorName) {

        this.DoctorName = DoctorName;
    }

    public String getDoctorprofile(){

        return Doctorprofile;
    }
    public void setDoctorprofile(String Doctorprofile) {

        this.Doctorprofile= Doctorprofile;
    }

}