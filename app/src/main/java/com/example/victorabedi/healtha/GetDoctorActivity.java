package com.example.victorabedi.healtha;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class GetDoctorActivity extends AppCompatActivity {
    ImageView me, us, them, we, they;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_doctor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Get Doctor");
        me = (ImageView) findViewById(R.id.overflow);
        me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(me);
            }

        });
        us = (ImageView) findViewById(R.id.overflow);
        us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(us);
            }

        });
        them = (ImageView) findViewById(R.id.overflow);
        them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(them);
            }

        });
        we = (ImageView) findViewById(R.id.overflow);
        we.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(we);
            }

        });
        they = (ImageView) findViewById(R.id.overflow);
        they.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You clicked yes button", Toast.LENGTH_LONG).show();
                showPopupMenu(they);
            }

        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(getApplicationContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_doctor, popup.getMenu());
        popup.setOnMenuItemClickListener(new GetDoctorActivity.MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(getApplicationContext(), "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_consult:
                    Toast.makeText(getApplicationContext(), "Consult Doctor", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_appoint:
                    Toast.makeText(getApplicationContext(), "Book Appointment", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }
}