package com.example.victorabedi.healtha;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PromoCode extends AppCompatActivity {
    public ImageView  overflow;
    TextView code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
code=(TextView) findViewById(R.id.code);
        String meme= code.getText().toString();
        overflow = (ImageView) findViewById(R.id.overflow);
        overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(overflow);
            }
        });
    }
    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(PromoCode.this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_promo, popup.getMenu());
        popup.setOnMenuItemClickListener(new PromoCode.MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_promoshare:
                    Toast.makeText(PromoCode.this, "Share With Friends", Toast.LENGTH_SHORT).show();
                    Intent share= new Intent(Intent.ACTION_SEND);
                    share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT,"Hi pal ,Download The HealthCare App on Google Play Store or use Link : www.healthcare.co.ke/app. Use my promocode to get 500point. Code:");
                    startActivity(share);
                    return true;
                case R.id.action_mainweb:
                    Toast.makeText(PromoCode.this, "Main Website", Toast.LENGTH_SHORT).show();
                    Intent site = new Intent(Intent.ACTION_VIEW, Uri.parse("http://apphive.co.ke/healthcareke"));
                    startActivity(site);
                    return true;
                default:
            }
            return false;
        }
    }

}
